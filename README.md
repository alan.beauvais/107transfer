# 107transfer

Mathematic epitech's project.

The transfer functions of these components
are rational functions, i.e. fractions such as both the numerator and the denominator are polynomials:

>![\Large f(x)=\frac{a_{n}x^{n}+... +a_{2}x^{2}+a_{1}x+a_{0}}{b_{m}x^{m}+... +b_{2}x^{2}+b_{1}x+b_{0}}](https://latex.codecogs.com/gif.latex?f%28x%29%3D%5Cfrac%7Ba_%7Bn%7Dx%5E%7Bn%7D&plus;...%20&plus;a_%7B2%7Dx%5E%7B2%7D&plus;a_%7B1%7Dx&plus;a_%7B0%7D%7D%7Bb_%7Bm%7Dx%5E%7Bm%7D&plus;...%20&plus;b_%7B2%7Dx%5E%7B2%7D&plus;b_%7B1%7Dx&plus;b_%7B0%7D%7D)



## Usage
```
USAGE
    ./107transfer [num denum]*

DESCRIPTION
    num    polynomial numerator defined by its coefficients
    denum  polynomial denominator defined by its coefficients
```
