#!/bin/usr/python3

def is_valid_str(string):
    i = 0
    while (i < len(string)):
        if ((string[i] < '0' or string[i] > '9') and string[i] != '*' and string[i] != '-'):
            print("invalid argument")
            exit(84)
        i += 1