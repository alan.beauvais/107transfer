#!/usr/bin/python3

import math

def my_calc(av):
    x = 0.0

    while (x < 1.001):
        a = 1
        result = 1.0
        while (a < len(av)):
            try:
                num = [int(s) for s in av[a].split('*')]
                denum = [int(s) for s in av[a + 1].split('*')]
            except:
                print("invalid arguments")
                exit(84)
            num_result = calc_poly(x, num)
            denum_result = calc_poly(x, denum)
            try:
                buffer = num_result / denum_result
            except:
                print("divide by 0")
                exit(84)
            result *= buffer
            a += 2
        print("%.3f -> %.5f" % (x, result))
        x += 0.001

def calc_poly(x : float, polynom)->float:
    i = 0
    result = 0.0

    while (i < len(polynom)):
        if (polynom[i] == 0):
            i += 1
            continue
        result += polynom[i] * math.pow(x, i)
        i += 1
    return result