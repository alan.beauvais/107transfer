#!/usr/bin/python3

def print_helper(name):
    print("USAGE")
    print("\t" + str(name) + " [num den]*\n")
    print("DESCRIPTION")
    print("\tnum\tpolynomial numerator defined by its coefficients")
    print("\tden\tpolynomial denominator defined by its coefficients")
    exit(0)

def print_usage(name):
    print("Usage :\t" + str(name) + " [num den]*")